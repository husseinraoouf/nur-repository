{ stdenv
, lib
, fetchurl
, makeWrapper
, libuuid
, libunwind
, icu
, openssl
, zlib
, curl
, at-spi2-core
, at-spi2-atk
, gnutar
, atomEnv
, kerberos
, makeDesktopItem
}:

stdenv.mkDerivation rec {

  pname = "azuredatastudio";
  version = "1.17.1";
  executableName = "azuredatastudio";
  longName = "Azure Data Studio";
  shortName = "Azure Data Studio";


  desktopItem = makeDesktopItem {
      name = executableName;
      desktopName = longName;
      comment = "Code Editing. Redefined.";
      genericName = "Text Editor";
      exec = "${executableName} %U";
      icon = "@out@/share/pixmaps/azuredatastudio.png";
      startupNotify = "true";
      categories = "Utility;TextEditor;Development;IDE;";
      mimeType = "text/plain;inode/directory;";
      extraEntries = ''
        StartupWMClass=${shortName}
        Actions=new-empty-window;
        Keywords=azuredatastudio;
        [Desktop Action new-empty-window]
        Name=New Empty Window
        Exec=${executableName} --new-window %F
        Icon=@out@/share/pixmaps/azuredatastudio.png
      '';
    };

  src = fetchurl {
    url = "https://azuredatastudiobuilds.blob.core.windows.net/releases/${version}/azuredatastudio-linux-${version}.tar.gz";
    sha256 = "130dd650750614d05a79726e50c6a9b181d66cfca09d4e146bcd6fe977b2a95f";
  };

  nativeBuildInputs = [
    makeWrapper
  ];

  buildInputs = [
    libuuid
    at-spi2-core
    at-spi2-atk
  ];

  # change this to azuredatastudio-insiders for insiders releases
  edition = "azuredatastudio";
  targetPath = "$out/lib/azuredatastudio";


  sqltoolsserviceRpath = stdenv.lib.makeLibraryPath [
    stdenv.cc.cc
    libunwind
    libuuid
    icu
    openssl
    zlib
    curl
  ];

  sqltoolsservicePath = "${targetPath}/resources/app/extensions/mssql/sqltoolsservice/Linux/2.0.0-release.56";

  rpath = stdenv.lib.concatStringsSep ":" [
    atomEnv.libPath
    (
      stdenv.lib.makeLibraryPath [
        libuuid
        at-spi2-core
        at-spi2-atk
        stdenv.cc.cc.lib
        kerberos
      ]
    )
    targetPath
    sqltoolsserviceRpath
  ];

  phases = "unpackPhase installPhase fixupPhase";

  unpackPhase = ''
    mkdir -p ${targetPath}
    ${gnutar}/bin/tar xf $src --strip 1 -C ${targetPath}
  '';

  installPhase = ''
    cd $out
    mkdir -p $out/bin $out/share/applications

    substitute $desktopItem/share/applications/${executableName}.desktop $out/share/applications/${executableName}.desktop \
      --subst-var out

    mkdir -p $out/share/pixmaps
    cp $out/lib/azuredatastudio/resources/app/resources/linux/code.png $out/share/pixmaps/azuredatastudio.png
  '';
  
  fixupPhase = ''
    fix_sqltoolsservice() 
    {
      mv ${sqltoolsservicePath}/$1 ${sqltoolsservicePath}/$1_old
      patchelf \
        --set-interpreter "${stdenv.cc.bintools.dynamicLinker}" \
        ${sqltoolsservicePath}/$1_old

      makeWrapper \
        ${sqltoolsservicePath}/$1_old \
        ${sqltoolsservicePath}/$1 \
        --set LD_LIBRARY_PATH ${sqltoolsserviceRpath}
    }

    fix_sqltoolsservice MicrosoftSqlToolsServiceLayer
    fix_sqltoolsservice MicrosoftSqlToolsCredentials
    fix_sqltoolsservice SqlToolsResourceProviderService

    patchelf \
      --set-interpreter "${stdenv.cc.bintools.dynamicLinker}" \
      ${targetPath}/${executableName}
      

    # Override the previously determined VSCODE_PATH with the one we know to be correct
    sed -i "/ELECTRON=/iVSCODE_PATH='$out/lib/azuredatastudio'" ${targetPath}/bin/${executableName}
    grep -q "VSCODE_PATH='$out/lib/azuredatastudio'" ${targetPath}/bin/${executableName} # check if sed succeeded

    substituteInPlace ${targetPath}/bin/${executableName} --replace '"$CLI" "$@"' '"$CLI" "--skip-getting-started" "$@"'

    makeWrapper \
      ${targetPath}/bin/${executableName} \
      $out/bin/azuredatastudio \
      --set LD_LIBRARY_PATH ${rpath}
  '';

  meta = {
    description = "A data management tool that enables working with SQL Server, Azure SQL DB and SQL DW";
    homepage = "https://docs.microsoft.com/en-us/sql/azure-data-studio/download-azure-data-studio";
    license = lib.licenses.unfreeRedistributable;
    platforms = [ "x86_64-linux" ];
  };
}
