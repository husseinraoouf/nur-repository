{ fetchurl, stdenv, nur }:

let

  packages = import ./generated-firefox-addons.nix {
    inherit fetchurl stdenv;
    inherit (nur.repos.rycee.firefox-addons) buildFirefoxXpiAddon;
  };

in

packages
