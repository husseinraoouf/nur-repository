{pkgs, stdenv, rustPlatform, fetchFromGitHub, system, nodejs, nur, makeRustPlatform, recurseIntoAttrs}:
let
  nodePackages = import ./node2nix-default.nix {
    inherit pkgs system nodejs;
  };

  myRustPlatform = recurseIntoAttrs(makeRustPlatform {
    rustc = nur.repos.mozilla.latest.rustChannels.stable.rust;
    cargo = nur.repos.mozilla.latest.rustChannels.stable.rust;
  });
in
nodePackages // {
  "@prisma/cli" = nodePackages."@prisma/cli-alpha".override rec {

    query-engine = myRustPlatform.buildRustPackage rec {
      pname = "query-engine";
      version = nodePackages."@prisma/cli-alpha".version;

      src = fetchFromGitHub {
        owner = "prisma";
        repo = "prisma-engines";
        rev = "455a3cf6bd2bc9dcb5069289187fe4f01d0047c9";
        sha256 = "sha256:16ais066cmkb2bcdkda5mzdklryclq1vqi4r3lqz3f85q1ix6xcs";
      };

      cargoSha256 = "sha256:0hrfnng7hy07c42b70malb6pcw92r189afkfbmi91rfllcpqwgg8";

      checkPhase = null;

      buildInputs = with pkgs; [ openssl pkgconfig ];

      meta = with stdenv.lib; {
        description = "A fast line-oriented regex search tool, similar to ag and ack";
        homepage = "https://github.com/BurntSushi/ripgrep";
        license = licenses.unlicense;
        platforms = platforms.all;
      };
    };


    preRebuild = ''
      cp ${query-engine}/bin/introspection-engine $out/lib/node_modules/@prisma/cli/introspection-engine-linux-nixos
      cp ${query-engine}/bin/migration-engine $out/lib/node_modules/@prisma/cli/migration-engine-linux-nixos
      cp ${query-engine}/bin/prisma-fmt $out/lib/node_modules/@prisma/cli/prisma-fmt-linux-nixos
      cp ${query-engine}/bin/query-engine $out/lib/node_modules/@prisma/cli/query-engine-linux-nixos
    '';

  };

}