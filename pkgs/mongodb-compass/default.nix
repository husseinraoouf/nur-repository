{ 
  stdenv, fetchurl, gcc-unwrapped, dpkg, makeWrapper, wrapGAppsHook

  # Linked dynamic libraries.
, alsaLib
, atk
, cairo
, cups
, curl
, dbus
, expat
, fontconfig
, freetype
, glib
, gnome2
, gnome3
, libnotify
, nspr
, nss
, systemd

, xlibs
, at_spi2_atk
, at-spi2-core
, libsecret
, libuuid
}:

stdenv.mkDerivation rec {
  pname = "mongodb-compass";
  version = "1.21.2";

  buildInputs = [
    dpkg
  ];

  src = fetchurl {
    url = "https://downloads.mongodb.com/compass/mongodb-compass_${version}_amd64.deb";
    sha256 = "0cea6f1e3339e7eb184747e4de70b8494f03d3497f544b8b9ff2688cd89c16b0";
  };

  dontBuild = true;
  dontConfigure = true;

  nativeBuildInputs = [ makeWrapper wrapGAppsHook ];

  rpath = stdenv.lib.makeLibraryPath [
    stdenv.cc.cc.lib

    alsaLib
    atk
    cairo
    cups
    curl
    dbus
    expat
    fontconfig
    freetype
    glib
    gnome2.GConf
    gnome2.gdk_pixbuf
    gnome3.gtk
    gnome2.pango
    libnotify
    xlibs.libxcb
    nspr
    nss
    stdenv.cc.cc
    systemd

    xlibs.libxkbfile
    xlibs.libX11
    xlibs.libXcomposite
    xlibs.libXcursor
    xlibs.libXdamage
    xlibs.libXext
    xlibs.libXfixes
    xlibs.libXi
    xlibs.libXrandr
    xlibs.libXrender
    xlibs.libXtst
    xlibs.libXScrnSaver
    at_spi2_atk
    at-spi2-core
    libsecret
    libuuid
  ];



  unpackPhase = ''
    dpkg --fsys-tarfile $src | tar -x --no-same-permissions --no-same-owner
    mkdir -p $out
    mv usr/* $out
  '';


  installPhase = ''
    runHook preInstall

    cd $out    

    pwd
    ls
    
    substituteInPlace $out/share/applications/mongodb-compass.desktop \
      --replace 'Exec=mongodb-compass %U' 'Exec=${pname} %U'

    substituteInPlace $out/share/applications/mongodb-compass.desktop \
      --replace 'Icon=mongodb-compass' "Icon=$out/share/pixmaps/mongodb-compass.png"
    
    runHook postInstall
  '';

  postFixup = ''

    patchelf \
      --set-interpreter "${stdenv.cc.bintools.dynamicLinker}" \
      $out/lib/mongodb-compass/MongoDB\ Compass

    makeWrapper $out/lib/mongodb-compass/MongoDB\ Compass $out/bin/mongodb-compass \
      --set LD_LIBRARY_PATH "${rpath}"

    wrapGAppsHook $out/bin/mongodb-compass
  '';

  meta = with stdenv.lib; {
    description = "The GUI for MongoDB";
    homepage = "https://www.mongodb.com/products/compass";
    license = licenses.unfree;
    platforms = [ "x86_64-linux" ];
  };
}