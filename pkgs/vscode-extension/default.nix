{ pkgs }:

{
    gitlens = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
          name = "gitlens";
          publisher = "eamodio";
          version = "10.2.0";
          sha256 = "d902e434d3135fe296bb4fcd0bf52da4f28ce4fa60fbc894d740824a324dd862";
      };
    };

    vscode-docker = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
          name = "vscode-docker";
          publisher = "ms-azuretools";
          version = "0.8.2";
          sha256 = "c21114a49586bb8e1d5887da8cc0ec4ac1ab4137bbbb77edba68edabbb100906";
      };
    };
}