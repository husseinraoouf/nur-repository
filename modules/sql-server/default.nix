{ config, lib, pkgs, ... }:

let

  cfg = config.services.sql-server;

in
{

  ###### interface

  options = {

    services.sql-server = {

      enable = lib.mkEnableOption "SQL Server";

      port = lib.mkOption {
        type = lib.types.str;
        default = "1433";
        description = ''
          The port on which SQL Server listens.
        '';
      };

      dataDir = lib.mkOption {
        type = lib.types.str;
        default = "/var/opt/mssql/data";
        example = "/var/opt/mssql/data";
        description = ''
          Data directory for SQL Server.
        '';
      };

      saPassword = lib.mkOption {
        type = lib.types.str;
        example = "123456As@";
        description = ''
          SA User Password.
        '';
      };

    };

  };


  ###### implementation

  config = lib.mkIf cfg.enable {

    docker-containers.sqlserver = {
        image = "mcr.microsoft.com/mssql/server";
        ports = [
            "${cfg.port}:1433"
        ];
        environment = {
            ACCEPT_EULA= "Y";
            SA_PASSWORD = cfg.saPassword;
        };

        extraDockerOptions = [
            "-v"
            "${cfg.dataDir}:/var/opt/mssql/data"
        ];
        
    };
  
    systemd.services."docker-sqlserver".after = [ "multi-user.target" ];
  };

}
