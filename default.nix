{ pkgs ? import <nixpkgs> {} }:

rec {

  modules = import ./modules;
  
  azuredatastudio = pkgs.callPackage ./pkgs/azuredatastudio { };

  mongodb-compass = pkgs.callPackage ./pkgs/mongodb-compass { };

  firefox-addons = pkgs.recurseIntoAttrs (pkgs.callPackage ./pkgs/firefox-addons { });

  vscode-extension = pkgs.recurseIntoAttrs (pkgs.callPackage ./pkgs/vscode-extension { });

  node-packages = pkgs.recurseIntoAttrs (pkgs.callPackage ./pkgs/node-packages { });
}